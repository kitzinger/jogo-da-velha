//Declaração de três variáveis auxiliares
var cont=1
var ganha=1
var contador=0

// Realiza uma jogada
function selecionar(botao) {
    if(ganha==1){
    //Se ninguém tiver ganhado ainda, roda o seguinte código
        if(botao.innerHTML == " "){
            if(cont==1){
                //Como, cont é igual a 1, O botão recebe X
                botao.innerHTML = "X"
                //O contador zera para alternar entre X e O
                cont=0
                //A vez é alternada
                document.getElementById("indicadorDaVez").innerHTML = "O"
                //Registra quantas jogadas foram feitas
                contador++            
            }else{
                //Como agora a variável cont é igual a 0, o botão recebe O
                botao.innerHTML = "O"
                //Cont retorna ao valor inicial para alternar a vez
                cont=1
                //A vez é alternada
                document.getElementById("indicadorDaVez").innerHTML = "X"
                //Registra mais uma jogada feita
                contador++  
            }
            //aplica a função de ganhou
            ganhou()
        }
    }
}

// Zera todos as posições e recomeça o jogo
casa = document.getElementsByClassName("casa")
function resetar(){
    //Repete o código 9 vezes, percorrendo todo os elementros da matriz composta pelos botões
    for(i=0;i<9;i++){
        //O botão fica em branco
        casa[i].innerHTML = " "
    } 
     //O indicador de vencedor fica em branco
    document.getElementById("indicadorVencedor").innerHTML = " "
    //Volta a não ter nenhum ganhador registrado na variável
    ganha=1
    //Contador de jogadas volta a 0
    contador = 0
}

// Verifica todos as combinações possíveis de se ganhar o jogo
function ganhou() {

    //Se a primeira fileira for toda igual, roda
    if( (casa[0].innerHTML==casa[1].innerHTML) && (casa[0].innerHTML==casa[2].innerHTML) ){
        if(casa[0].innerHTML== "X"){
            //Se os botões iguais contiverem X, então o indicador de vencedor recebe X
            document.getElementById("indicadorVencedor").innerHTML = "O vencedor é X"
            //Variável auxiliar registra que já existe um ganhador
            ganha=0
        }
        if(casa[0].innerHTML== "O"){
            //Se os botões iguais contiverem O, então o indicador de vencedor recebe O
            document.getElementById("indicadorVencedor").innerHTML = "O vencedor é O"
            ganha=0
        }
    }

    //Se a segunda fileira for toda igual, roda
    if( (casa[3].innerHTML==casa[4].innerHTML) && (casa[3].innerHTML==casa[5].innerHTML) ){
        if(casa[3].innerHTML== "X"){
            //Se os botões iguais contiverem X, então o indicador de vencedor recebe X
            document.getElementById("indicadorVencedor").innerHTML = "O vencedor é X"
            //Variável auxiliar registra que já existe um ganhador
            ganha=0
        }
        if(casa[3].innerHTML== "O"){
            //Se os botões iguais contiverem O, então o indicador de vencedor recebe O
            document.getElementById("indicadorVencedor").innerHTML = "O vencedor é O"
            //Variável auxiliar registra que já existe um ganhador
            ganha=0
        }
    }

    //Se a terceira fileira for toda igual, roda
    if( (casa[6].innerHTML==casa[7].innerHTML) && (casa[6].innerHTML==casa[8].innerHTML) ){
        if(casa[6].innerHTML== "X"){
            //Se os botões iguais contiverem X, então o indicador de vencedor recebe X
            document.getElementById("indicadorVencedor").innerHTML = "O vencedor é X"
            //Variável auxiliar registra que já existe um ganhador
            ganha=0
        }
        if(casa[6].innerHTML== "O"){
            //Se os botões iguais contiverem O, então o indicador de vencedor recebe O
            document.getElementById("indicadorVencedor").innerHTML = "O vencedor é O"
            //Variável auxiliar registra que já existe um ganhador
            ganha=0
        }
    }

    //Se a primeira coluna for toda igual, roda
    if( (casa[0].innerHTML==casa[3].innerHTML) && (casa[0].innerHTML==casa[6].innerHTML) ){
        if(casa[0].innerHTML== "X"){
            //Se os botões iguais contiverem X, então o indicador de vencedor recebe X
            document.getElementById("indicadorVencedor").innerHTML = "O vencedor é X"
            //Variável auxiliar registra que já existe um ganhador
            ganha=0
        }
        if(casa[0].innerHTML== "O"){
            //Se os botões iguais contiverem O, então o indicador de vencedor recebe O
            document.getElementById("indicadorVencedor").innerHTML = "O vencedor é O"
            //Variável auxiliar registra que já existe um ganhador
            ganha=0
        }
    }

    //Se a segunda coluna for toda igual, roda
    if( (casa[1].innerHTML==casa[4].innerHTML) && (casa[4].innerHTML==casa[7].innerHTML) ){
        if(casa[1].innerHTML== "X"){
            //Se os botões iguais contiverem X, então o indicador de vencedor recebe X
            document.getElementById("indicadorVencedor").innerHTML = "O vencedor é X"
            //Variável auxiliar registra que já existe um ganhador
            ganha=0
        }
        if(casa[1].innerHTML== "O"){
            //Se os botões iguais contiverem O, então o indicador de vencedor recebe O
            document.getElementById("indicadorVencedor").innerHTML = "O vencedor é O"
            //Variável auxiliar registra que já existe um ganhador
            ganha=0
        }
    }

    //Se a terceira coluna for toda igual, roda
    if( (casa[2].innerHTML==casa[5].innerHTML) && (casa[2].innerHTML==casa[8].innerHTML) ){
        if(casa[2].innerHTML== "X"){
            //Se os botões iguais contiverem X, então o indicador de vencedor recebe X
            document.getElementById("indicadorVencedor").innerHTML = "O vencedor é X"
            //Variável auxiliar registra que já existe um ganhador
            ganha=0
        }
        if(casa[2].innerHTML== "O"){
            //Se os botões iguais contiverem O, então o indicador de vencedor recebe O
            document.getElementById("indicadorVencedor").innerHTML = "O vencedor é O"
            //Variável auxiliar registra que já existe um ganhador
            ganha=0
        }
    }

    //Se a primeira diagonal for toda igual, roda
    if( (casa[0].innerHTML==casa[4].innerHTML) && (casa[4].innerHTML==casa[8].innerHTML) ){
        if(casa[0].innerHTML== "X"){
            //Se os botões iguais contiverem X, então o indicador de vencedor recebe X
            document.getElementById("indicadorVencedor").innerHTML = "O vencedor é X"
            //Variável auxiliar registra que já existe um ganhador
            ganha=0
        }
        if(casa[0].innerHTML== "O"){
            //Se os botões iguais contiverem O, então o indicador de vencedor recebe O
            document.getElementById("indicadorVencedor").innerHTML = "O vencedor é O"
            //Variável auxiliar registra que já existe um ganhador
            ganha=0
        }
    }
    
    //Se a segunda diagonal for toda igual, roda
    if( (casa[2].innerHTML==casa[4].innerHTML) && (casa[4].innerHTML==casa[6].innerHTML) ){
        if(casa[2].innerHTML== "X"){
            //Se os botões iguais contiverem X, então o indicador de vencedor recebe X
            document.getElementById("indicadorVencedor").innerHTML = "O vencedor é X"
            //Variável auxiliar registra que já existe um ganhador
            ganha=0
        }
        if(casa[2].innerHTML== "O"){
            //Se os botões iguais contiverem O, então o indicador de vencedor recebe O
            document.getElementById("indicadorVencedor").innerHTML = "O vencedor é O"
            //Variável auxiliar registra que já existe um ganhador
            ganha=0
        }
    }

    //Se já tiverem sido efetuadas nove jogadas e ninguém tiver ganhado ainda, roda
    if( (contador==9) && (ganha==1) ){
        //O indicador de vencedor recebe que deu velha
        document.getElementById("indicadorVencedor").innerHTML = "Deu velha!"
    }

}

